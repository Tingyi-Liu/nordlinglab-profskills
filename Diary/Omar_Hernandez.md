*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*

# 2019-09-12

## 1st Lecture

On this first lecture we got an introduction to the course and didn't expect to be that interesting. The exam took me by surprise but it serves 
to show how much one knows about the world. I hope I can answer all of the questions by the end of the semester and I look forward to learning 
a lot. 

> Omar Hernandez


*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).* 

# 2019-09-19

## 2nd Lecture 

This is hands down the best lecture I have ever taken at NCKU. The professor's pedagogies are geared towards learning.
I can understand what is going on in class and I really learn a lot of important concepts that as future engineers we 
should learn but sadly NCKU offers no course to teach us or even bother to put it in our curriculum. 

On this second lecture I learned about Wright's Law, which basically states that as a product's number of unit production 
increases it's cost of production decreases. I had only heard of Moore's Law but this the former I feel applies to more 
products. The video we saw was also eye-opening as the speaker used data to debunk the myth that our world is getting 
worse and worse by year as portrayed by news with all the murders,natural disasters deaths, climate change problems etc.
But in fact our world is doing much better than the past, although of course we still have problems to face and try to solve 
such as eradicating diseases and tyring to eradicate poverty.

Lastly, we were introducted to GIT, a version controlled software. I think it is a right time to get exposed to these type 
of software so that when we go out there in the real world we already know about these things actually used in companies 
and industries not the mere textbook theory and proofs that are useless. I still have to spend a lot of time practicing 
and getting familiar with GIT but at first thought I think it's really a nice way of keeping track of changes and 
lends itself for collaboration. 

> Omar Hernandez


*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*

# 2019-09-26

## 3rd Lecture

Today's lecture was quite eye-opening for me. I had always assumed, so to speak, that whatever news that was out there could 
be trusted because news are meant to be objective and credible. But the TED videos we watched today made me think otherwise. 
I really hadn't had the thought that it is good practice to always question news. That is, do your own research to verify that 
what the news medium claims is indeed true. 

The lecturer also told us about the 6 honest service men: What, Where, When, Who How, and Why. We can use these as guidelines to
question the sources of our news and figure out whether it's true or it's fake news. I think from now on, I will be more attentive 
of where I can my news from and only trust sources after cross checking and verifying their credibility and accuracy of the news.

> Omar Hernandez


*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*

# 2019-10-03

## 4th Lecture

* Economy is the sum of transactions that make it up.
* Total spending drives economy.
* Borrowing creates cycles.
* If you have no loans you have to increase your productivity. 
* Increased productivity leads to increased income. 
* Central banks print money but leads to inflation. 
* Central bank can only buy financial assets. 
* TED Video:

    - Banks create money.
	- Deposit to bank is no more than a loan to the bank.
	- Or a record of the loan that the bank does from the public.
	- i.e. Banks loan from public (when you deposit) and then loan to other people. 
	- No money is actually transfered. 

> Omar Hernandez


*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*

# 2019-10-17

## 5th Lecture

* Nationalist countries are more peaceful than non-Nationalist countries 
* Facism- What nation demands thats what you do.
* Facism- Teach kids whatever serves the nation even if it’s against truth.
* Land was valuable in the 1930s. Then it’s machines , now data. 
* AI and machine learning will help centralized data processing to become more efficient 
* rather than distributed data processing. 
* Democracy can become puppet show because of feelings not our thoughts.

> Omar Hernandez


*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-10-24

## 6th Lecture

* Simply moving your body can help your brain.
* Prefontal cortex - decision making, foucs.
* Hippocampus - memories.
* 30 mins of exercise (3-4 times per week) can have long lasting benefits:
 
   - long term memory
   - energy
   - attention
  
* 30% of surgical procedures were conducted without considering non-surgical options first.
* Paradigm shift - treating sick -> helping the healthy not to get sick. 
* Why are governments not focusing on better health care systems? Healthy nations-> better economy?
* Is Social Security Board (Belize) a true health care system? You pay and pay but never "qualify" for refunds.
* Why Belizeans dont get a cheaper price at health centers with SSB contributions? We should have an insurance card like Taiwan?
 
> Omar Hernandez




*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-10-31

## 7th Lecture

* Is depression being sad?
* Depression is the #1 cause of illness.
* In US 7% of people are depressed
* Just be friendly to depressed people. 
* Golden Gate Bridge is known for suicides. 
* Listen to undersand. 
* Don't argue, blame or tell them the way you feel. 
* How much people survive from leaping the Golden Bridge?

> Omar Hernandez




*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-11-07

## 8th Lecture

* Very provoking question: Do you really know why you do what you do?
* Is manipulating people for scientific research right?
* Did all the participants behave the way they predicted?
* The scientific journals under go a peer-review process not for it to be accurate 
  information but so people are interested.
* Use rhetorics for presentation;they are powerful! -> pathos, ethos and logic 
* Learn to forgive. 
> Omar Hernandez



*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-11-14

## 9th Lecture

* Debt collectors have no power. They have as much power as you give them. 
* We individuals are sovereign and free. 
* With knowledge comes power. 
* Government is NOT above man because it was created by man. 
* Legal fiction -> strawman 
* Corporation -> corp-dead; oration-speak 
* State owns you at birht when your parents register you and you get a birth certificate. 
* Lawful -> orginal law, law of land
* Legal -> system, acts, statues, rules of society 
* Common law -> lawful or nothing criminal
* Act of Parliament/Congress (UK/USA)
* You don't have to give your details to police if you haven't committed a crime. 
* "Innocent until proven guilty".
* "Are you hounouring your oath?"
* Not illegal to film in public or film police. 
* We are born free!

> Omar Hernandez



*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-11-21

## 10th Lecture

* "Behaviour modification empires" not social networks. Agree
* Dopamine- makes you feel good
* Dopamine makes things addictive becuase of the above. 
* Instant gratification
* Algorithm creators define success for themselves.
* Bias
* Codified Opinion
* Algorithms are not supposed to be used as decision makers. E.g. teacher example 
* Good team-member skills are essential in real life. 

> Omar Hernandez









*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-11-21

## 10th Lecture

* "Behaviour modification empires" not social networks. Agree
* Dopamine- makes you feel good
* Dopamine makes things addictive becuase of the above. 
* Instant gratification
* Algorithm creators define success for themselves.
* Bias
* Codified Opinion
* Algorithms are not supposed to be used as decision makers. E.g. teacher example 

> Omar Hernandez



*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-11-29

## 11th Lecture

* When watching news get in the habit of looking up various sources. 
* Piece it out using all the variations of the news outlets.
* We discussed how difficult it is to draw the line when coming up with a new law. 
* We learned that the same mistakes that have been made before are still made again.
* Leaders (government) don't read monetary history.
* Goverments shouldn't hurry to intervene during a depression.
* The market will heal itself eventually. 
* Money lost is 1:1 value to gold and things went bad. 

> Omar Hernandez




*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-12-05

## 12th Lecture

* We learned about Planetary Boundaries
* There are nine of them. 
* Some boundaries are not quantifiable. 
* Some boundaries we have surpased the "safe" boundary. 
* Ozone layer boundary was surpassed a few years ago but brought back to a safe boundary. 
* From 2010 (or before) some scientists have started to 'bend' the curves. 
* What would happen if we surpasses all the safe boundaries?
* How long would we survive?
* How did they come up with these boundaries, and why only 9?
* Are there other factors apart from these 9 that are essential to our survival?


> Omar Hernandez




*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-12-12

## 13th Lecture

* Today's lectures was a bit different.
* More discussion based; I think it's great! 
* I learnt a lot from the discussion.
* Being able to defend my planetary boundary by answering questions led me to learn a lot about the topic while preparing for the debate. 
* I learned that our planet has certain boundaries that we need to be careful about being in the safe zone. 
* Our existence depends on us!
* Wish my major had classes like this where I could participate and laern through discussion. 
* Not sitting down 3 hours and listening to the teacher read a PPT.


> Omar Hernandez


*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-12-19

## 14th Lecture

## 19/12/19
* A. unpoductive and unsuccessful 
* B. I felt unproductive because I stayed up late to watch football game and hence didnt get enough sleep.
	     When I dont enough sleep I cant focus and study as my head hurts.I felt unsuccessful because I felt like I just 
		 wasted my day.
* C. I will make sure to get enough sleep the night before so I can focus and be productive.


## 19/12/20
*  A. unpoductive and unsuccessful 
*  B. I felt unproductive because it was a cold and rainy day. It made me lazy to do any work. I felt unsuccessful 
		because had 5 hours of class and basically was a waste of time. Can’t study in class if professors are talking loud.
*  C. I will make sure to wake up early instead to study and schedule my day the night before.


## 19/12/21
*  A. unpoductive and unsuccessful 
*  B. Traveled for a Christmas activity. Didn't do anything at all so I felt unsuccessful/unproductive.
*  C. I will try to read a bit while on train to not waste time.


## 19/12/22
*  A. unpoductive and unsuccessful 
*  B. Traveled back home from Christmas activity. Was so tired I didn't do anything also. Felt 
		so unsuccessful/unproductive.
*  C. I will try to read a bit while on train to not waste time instead of listenting to music.
	
	
## 19/12/23
* A. productive and successful 
* B. I felt successful/productive because I got a lot of things on my To-Do list done.  I managed my time properly 
		and still had time for exercise, plus I woke up early. 
* C. I will continue using the Pomodoro Technique.
	
## 19/12/24
*  A. productive and successful 
*  B. I felt successful/productive because I finished reading the chapter I had to read. I managed to start preparing for one of 
		my final exams and quiz this week. 
*  C. I will continue organizing my day the night before so I know exactly what I need to do the next day. 
	
## 19/12/25
*  A. successful and unproductive
*  B. I felt successful because I was able to wake up at the time I said I would wake up and finished an assignment ON a 
      Christmas day. 
	  But, I felt unproductive becuase I had a next 5 hours of class and it was just a waste of my time. 
*  C. I am not sure if going to 5 hours of lecture is really productive. Maybe I should go to library and read my book. I will 
      experiment next time. 
	
## 5 Rules to be Successful 
*  Focus on one task at a time. DO NOT multi-task.
*  Use the pomodoro technique.
*  Remove distractions. 
*  Listen to instrumental music only while studying.
*  Set up your day the night before. 
	
> Omar Hernandez



*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2019-12-26

## 15th Lecture

## 19/12/26
* A. unpoductive and unsuccessful 
* B. I felt unproductive because I was feeling in the Christmas spirit and these times are not to do work. Back home
     everyone is on holidays and celebrating.
* C. I will make do a little work at least because sadly Taiwan doesn't recognize Christmas and we still have classes.


## 19/12/27
*  A. unpoductive and successful 
*  B. I felt unproductive because it was a cold day. It made me lazy to do any work and plus it's Friday. I felt successful 
		because I did well on my Circuits quiz. 
*  C. I will try to exercise on a cold day next time. 


## 19/12/28
*  A. productive and successful
*  B. I felt productive and successful today because I was able to finish a final presentation within the time 
	  frame I gave myself.
*  C. I will try to keep up this productivity by waking up early.


## 19/12/29
*  A. unproductive and unsuccesful	
*  B. I felt unproductive and unsuccessful becuase I slept until the afternoon and I feel I wsted my day.
	  Maybe because I was feeling the New Years celebratory spirit. 
*  C. I need to sleep earlier the night before so I can wake up early.
	
	
## 19/12/30
* A. successful and productive
* B. I felt successful and productive because I started studying for my final exams all day despite wanting to 
	 just relax and celebrate new years.
* C. I need to keep strengthening my self-discipline. 
	
## 19/12/31
*  A.  productive and unsuccessful
*  B.  I felt productive because I was able to start another final presentation but felt unsuccessful because I wasn't 
       able to finish it as I had planned.
*  C. I need to try and stick to my plan and avoid distractions.
	
## 20/12/1
*  A. unproductive and unsuccessful
*  B. I felt unproductive and unsuccessful because it is new year's day and I took the day off to relax and celebrate
   the new year a bit.
*  C. I need to get back to reality tomorrow again.
> Omar Hernandez



*This diary file is written by Omar Hernandez (E24057031) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*
# 2020-01-02

## 16th Lecture

* professor clarified his reason for creating the course
* to "empower" us
* I think that is something many schools as a whole should do but fail; at least my department only focuses and 
  on grades and memorzing
* We need skills as engineers to survive the real world. We cannot depend of memorization and good grades out there
* Nobody cares! They care if you can solve the real problem or not and collaborate (among other important skills)
* Felt as if it's the last lecture since next week is final presentation and exam
* Great course! I would take other courses taught by this professor.
* For 3 hours a week I felt I was actually in universtiy-learning.
* The rest of my time is wasted with my Taiwanese professors.
* They need to take a course on how to be a teacher and scrap that memorizing and just reading out loud ppt stupidness.
* I feel a step closer to being a skilled engineer not just any engineer.

> Omar Hernandez
