This diary is written by Fernando Augustin E34075316 in the course "professional skills for engineering the third industrial revolution"

# 2019-09-26
* Today was my first lecture in this course
* At first I was overwhelmed by the unfamiliar system used in the course
* The group presentation was great and thought provoking, although it's a pity that all doesn't get to present
* Today's lecture main topic is primarily about fake news
* I do agree that fake news is really a problem, because it is really rampant from where I came from (Indonesia)
* I think it is interesting how fake news is allegedly used by the Russians as a casus belli for invading Ukraine

# 2019-10-03
* In today's lecture I primarily learned about the economics system
* I learned that money today is only worth what we think it is worth
* I also learned that all of the currency nowdays doesn't have a backing value like it used to like gold for instances.
* I also learned that US dollar is a really powerful currency and losing access to it is basically an economical death sentences.
* It's kind of depressing learning that the world's economy is rigged such that it will experience recession every once in a while.

# 2019-10-17
* In today's lecture I primarily learned about the emergence of extremism in the world, particulary fascism 
* I learned that people who were attracted to joining ultra-nationalist/neo-nazi/fascist movement are usually the one who are lonely and often lack in self identity 
* I completely agree with that, because it doesn't make sense for people to join such group in the first place
* However I disagree with one of the speaker who stated that nationalism and fascism are completely different, because in my opinion fasicsm itself is built on the sense of ultra-nationalism

# 2019-10-24
* In today's lecture I learned mostly about keeping the body healthy
* In my opinion the effect of excercise on one's mood was greatly exaggerated by one of the TED Talk speaker
* Because in my experience I still feel stressed even after working out
* The speaker also mentioned that excercise improve one's cognitive ability
* The problem with that statement is based on my personal observation, the more smarter kids are usually the one that does less excercise

# 2019-10-31
* In today's lecture I learned mostly about depression and mental illness
* In my opinion today's lecture doesn't put much emphasis on how to combat depression
* The TED talk video I watched in my opinion mostly talks about one's experience with depression, not treating them
* Although the video did give me a good picture on how depression actually looks like
* I was actually quite suprised at the prevalence of depression around the world
* And like what the professor stated, I believe that depression is also really prevalent at NCKU

# 2019-11-07
* In today's lecture I learned mostly about pursuing happiness 
* One of the video mentioned that the more you pursue happiness the less happy you will become
* I agree with this point, because eventually you will ironically becomed stressed from trying to obtain that happiness
* One of the other video also mentioned that forgiving is the way to go
* I don't quiet agree with this point being a very vengeful person 
* I am still perfectly happy eventhough I have commited a lot of acts of vengeance 

# 2019-11-14
* In today's lecture I learned mostly about the Legal system
* I was kinda suprised that legal authority actually doesn't have that much power
* I was also suprised by the fact that we as the people unknowingly consent to leave most of our freedoms in the hand of the goverment
* However I disagree with the video's way of dealing with police as it mostly antagonise the policemen
* In several countries this can be outright deadly since the police are authorized to shoot at people who they perceived as dangerous, in this case those who refuses to follow his order

# 2019-11-21
* In today's lecture I learned how to stay connected and free
* I personally think that the videos shown on today's lecture doesn't really relate to the topic
* I agree that prediction alogarithm is still not yet perfect, but with machine learning I think that's about to change
* To be honest I don't really see anything wrong with advertisement based revenue the internet is currently based on 
* And I also agree that smartphone addiction is really rampant these days 

# 2019-11-28
* Today's lecture was about on how to make sense of events
* I learned that small things happening during a duration of time will snowball into something big later on
* one of the video was about differentiating between opinion on facts
* However, I think most of them are common sense
* The real problem I think is, most people see what they want to see
* I also learned that multiple small factor, can even destroy civilization as a whole

# 2019-12-05
* Today's lecture was about the planetary boundaries
* The lecture began with the usual presentation and the discussion of the opposing views that were presented in the news analysis.
* Coming from Indonesia, I was surpised that gender equality is still a problem today.
* I think in my country this wasn't really a problem and many people generally agree that both men and women were treated equally.
* At least that was what I heard, I do know that women does face more harrassment from random people.
* However I do think that that was more of a criminal issue rather than something to do with gender equality.

# 2019-12-12
* Today's lecture was primarily spent debating
* I personally think that climate change is the most important planetary boundary
* Because it will affect how our agricultural system works
* It is no secret that our agricultural system today is build on fertilizer 
* Climate change may reduce the amount of land that is available to us to do agriculture on
* Since food is very important in society, thats why I think climate change is significant

# 2019-12-19
* Today's class was primarily spent on discussion questions related to the third industrial revolution
* I got to present my answers to the problem "How Industry 4.0 fits into the picture".
* After each group finished presenting their answer to the problem the class have a debate
* The class is divided into 3 groups each representing the environmentalist, workers and capitalist, each tries to reach a consensus on the problem "why does inequality exist"
* Then we watched a short video on how to be productive even if you are currently in an unmotivated state
* I think the video is interesting, and I would definitely like to try some of the methods suggested

# 2019-12-26
* Today's Lecture was primarily spent on the final presentation
* Beforehand, everyone got to present the rule that they made for the private homework
* The topic that my group presented was "Is fruit really healthy for us?"
* The class ended before everyone got a chance to present which is kind off a shame 
* The video the we are supposed to watch for today that intrigue me were the one discussing the aerosoil vertical farming
* I think this will really help humanity since we used up so much land for agriculture
* In the long run this can help safe the planet since there will be less forest being burned down for agricultural purposes

# 2020-01-02
* Today was the last day of class
* Today's lecture was primarily spent on discussion the 3 solutions that we have previously proposed
* After that we wacth a few motivational videos on how to make things go viral and social justice
* I personally don't quite like the video regarding the social justice and education system
* Because I usually find that people that are involved in it tend to be quite whiney 
* And to have everyone participate in democracy I believe is just going to cause anarchy
* For the how to make things go viral video, I quite enjoy it since it is backed by a lot of data and scientific findings
* My impression on the course have been generally positive, however I do think there is too much homework

# 2020-01-09
* Today was the final test and the last lecture for this course and also my last class for the semester
* I did quite well on the final exam in which i manage to score 90
* Before the exam though we get to watch two videos on the topic "love"
* One video was titled the mathematic of love and while the other was titled how to cure a heart break
* Personally I don't see how these things correllate with engineering
* I personally think that as time goes on the topic of this course really starts to stray away from the engineering aspect
* Personally it was heart break that got me to this point, because before experiencing "the event" I was a very lazy person
* However after that experience I "woke up" and manage to get my weight together and become productive

# Daily Diary
# 191219 Thurs
* Succesfull and productive
* I felt succesfull because this is the last day of class, and my next "real" class is on monday, I manage to survive school
* I felt productive because I finished 1/4 of my engineering math HW and finish studying an entire chapter of organic chemistry material
* I will study for engineering math to finish my homework
# 191220 Fri
* Unsuccesfull and productive
* I felt unsuccesfull because I performed really bad at today's ping pong class
* I felt productive because I finished studying 2 chapter of engineering math
* I will finish the engineering math homework then play
# 191221 Sat
* Succesfull and unproductive
* I felt succesfull because I finished my term presentation for another course
* I felt unproductive because I only did that for the day and play games for the rest of the day
* I will try waking up earlier
# 191222 Sun
* Succesfull and unproductive
* I felt succesfull because my parents and cousin praised me for earning a spot on the exchange student programme
* I felt unproductive because I only manage to did 3 number of engineering math HW and study 2 sub chapter of organic chemistry
* I will try to post notes to remind me of my task
# 191223 Mon
* Succesfull and productive
* I felt succesfull because I manage to do well (I think) on my organic chemistry lab finals
* I felt productive because I manage to finish another half of my engineering math homework
* I will try to finish the homework for real tommorow
# 191224 Tues
* Succesfull and productive
* I felt sucesfull because I manage to score good on my homework for another course
* I felt productive because I finally managed to finish my engineering math homework
* I will try to meditate after waking up to further concentrate on my class
# Five rules
* Don't blame yourself too much
* Don't overwork
* Don't procrastinate 
* Find the right motivation to do the things you want to do
* Always account for what could go wrong
