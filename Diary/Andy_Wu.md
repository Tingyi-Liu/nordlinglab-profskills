This diary file is written by Andy Wu E24085327 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* I've decided to continue on this course since it helped me improve my researching skill and get to understand what's going on in the world.
* For my group's previous presentation, I've learned not to give credits or mention a member's name if the person didn't contribute and is no longer a part of our group.
* I hope that this class would inspire me to see the opportunity and pathway to make in the future's society.