This diary file is written by YunShiou Chiou D84106035 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* This was the first time that I added in a course given by a foreign teacher. 
* I was confused about how to use git and bitbucket because it was my first time to use them.
* I was considering to drop the class because it was taught in English but what professor said in the class encouraged me to challenge myself.
* Being objective is not to treat everyone equally but to give everyone the equal chance to be listened.
* Stick to the information sources that you know or those are reliable.

