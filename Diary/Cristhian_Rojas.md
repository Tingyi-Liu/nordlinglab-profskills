## 2019-09-25 (2019-09-19)

Class 2

1. After rewatching the video and thinking about it 
some of the claims just  seems to be too good to be true

2. The claims that strucked the me most were the ones
about extreme poverty and happiness but the one
about happines didn't have a chart or data backing 
up the claim he just seemed to conclude it 

3. I don't know how you can make measurements on 
such a subjective thing as happiness 

## 2019-10-01 (2019-09-26)

Class 3

1. The class was mostly expositions of the groups 
with a lot of diverse subjects to me the most 
interesting one was about the polution

2. The videos that we watched were very interesting
but i disagree with the claim that the flow of information should 
be controled to avoid fake news 

## 2019-10-03 (2019-10-03)

1. hearing the different groups talking about 
fake news and how it spreads were very useful 
especially group 16 when talked about some tools 
to spot fake news

2. the teacher talked about how fake news its 
almost the same as fake news and i agree with 
his opinion


## 2019-10-23
 
1. the presentation about white supremacy was really interesting 
because in m  y country we don't have this kind of groups 

2. the video in about the immigration crisis was realy emotive but 
i left with questions like why does the parents decide to bring their 
children to cross the frontier if they know the conditions are so harash

3. the talk about the current situation in Hong Kong and China was realy 
eye opening. i left with the impression that the Taiwanese students 
were strongly impacted 

## 2019-10-24

1. the supergroup dynamic was very interesting it really helped me 
to understand what is a valid hypothesis

## 2019-10-31

1. The first video clarified a lot of misconceptions that i had about depression
and taugh me how to be a better friend to depressed people 

## 2019-11-07

1. the first video showed me how can my decisions can be 
manipulated 

2. in the second video i saw how the power of forgiveness
can break the cycle of violence and give people 
a second chance 

3. but we also  discussed some ways in wich forgiving its 
not enough or maybe not the best choice 

4. the last video talked about the difference between 
happiness and meaning 

5. it also talked about the pillars of a good life 
letting me know that the way i see my own life 
its integral in a meaningful existence


## 2019-11-14

9th lesson

* Check diary and listen to group's presentations
* Discussion by groups of internal characteristics and external factors someone wants to change
* See videos about british law and british police. It was a little bit confusing but is important to understant our rights as civilians


## 2019-11-21

1. i found the video about big data gave me a lot of 
insight on the use of these tools might affect our society and future

2. the video about millenials in the workplace semmed to point a reality of the millenials in
already developed countries

3. in my personal experience a lot of those points dont hold in other nations 

4. but also helped me understand a little bit better the dynamic in the workplace and 
some attitudes that i need to correct

## 2019-11-28

1. the video about the parallels between the USA and Rome was a really interesting point of view one that I had never seen before 

2. but I think it may oversimplify the reasons for the fall of the roman civilization taking only economics as the cause 

3. video about the Egyptians was unusual letting you know that a lot of the sparse knowledge that one has about that era can be interpreted
in a way that gives us a more defined and connected view 

## 2019-12-10 
1. the whole dynamic and presentations were extremely interesting 
2. It had not only information but also had the opinions of the presentations 
3. I thought that the debate will have more participation but the questions compensated for that 
4. the content of the class had to be given as homework but I will expand on that after watching the video

## 2019-12-19

191219 Thu

A. Successful and productive
B. I felt successful because I manage to study after class until the library closed. I felt productive because I did all my homework and slept 8 hours but in the library, I used a lot my phone.
C. I will keep studying in the library trying to control how much I use my phone.

• 191220 Fri

A. Successful and productive
B. I felt successful because I studied and did my lab report also managed to use my phone less.
C. I will get up tomorrow at 08:00 instead of 12:00 like last Saturday and try to implement the Pomodoro technique.

• 191221 Sat

A. Successful and unproductive
B. I felt successful because I managed to help and organize our Christmas party but unproductive because I didn't study at all and I woke up very late.
C. I will set multiples alarms to avoid oversleeping.

• 19222 Sun

A. Unsuccessful and unproductive
B. I felt unsuccessful because I overslept again and spend the whole day playing videogames but at least I managed to do my homework
C. I will ask my roommates to wake me up if don't wake up by myself also I will avoid using my phone late at night.

• 191223 Mon 
A. Successful and productive
B. I felt successful because I manage to go to the library and study but I overslept again waking up at 11:00, not at 8:00
C. I will try letting my phone at home to not use in the library.

• 191224 Tues 
A. unsuccessful and productive
B. I felt unsuccessful because didn't study at all but I felt productive because cleaned my house 
C. I will ask a classmate to study with me after class.

• 191225 Wed
A. unsuccessful and productive
B. I felt unsuccessful because I ate too much and then fell at sleep until 18:00 but I was productive because I manage to study for my exam and do my presentation 
C. I will avoid taking naps after eating lunch.

## 2019-12-26

1 The presentations of today were really interesting my favorite one was about cure aging
2 When my time to present came i was really tired and a little nervous but in the end i think it end up fine
3.I liked the dynamic about brainstorm and solutions 

## 2020-01-12

1 i liked most of the videos and presentations but some of them didn't seem to provide a good solution 
2 the exam was a very different experience because we already had been informed about most of the topics and the misconceptions that we during the course so preparing wasn't such a strain 
3 the videos about love were a little weird I always have separated the concept of love from math and the ideas shown were accurate to a degree because it didn't mention the rate of divorce of second and third marriages so overall the percentage is more than 50 percent 
4 the video about heart broke gave me a more wide perspective in the depths of love and how affects our personal lives

## homework 

1. Know the current world demographics
*This task can be more easy to do if we start by analyzing the demographic data of our country first 

2. Ability to use demographics to explain engineering needs 
* We can focus on how the automation is a viable Solution to an aging population (in terms of productivity)

3. Understand planetary boundaries and the current state 
* I really liked the way we did this during the course but i will add some personal information for example taiwan is contributing to the acidification of the ocean and how it can have an impact on taiwan 

4. Understand how the current economic system fail to distribute resources 

* I will add some information on game theory that would help us understand if the strategies are logical or not in a different context 

5. Be familiar with future visions and their implications, such as artificial intelligence Personal health, happiness, and society" 

* We can start a mini challenge in wich we have to reduce the amount of use of services like google maps or even our own cellphones to see how different a "disconected" can be 

6. Understand how data and engineering enable a healthier life 
*monitoring our own body through apps to see how usefull can be to know how many calories we burn 

7. Know that social relationships gives a 50% increased likelihood of survival 
* watch known cases of a lack of social interactions and the effects it produces 

8. Be famillar with depression and mental health issues 
*Maybe visit the university psychologist 

9. Know the optimal algorithm for finding a partner for life 
* Task the students to study past relationships to see any patern and disscus life plans in the class (when to get married and questions like that )

"Professional skills to success" 

10. Develop a custom of questioning claims to avoid "fake news"

*developt a mini game in wich we have to guess if a newsource is fake dubious or reliably

11. Be able to do basic analysis and interpretation of time series data
* showing us various tables in wich the data changes with time and ask us to explain the trends 


12. Experience of collaborative and problem based learning 
*maybe trying a game in wich we have to solve a puzzle in groups in a short amount of time 

13. Understand that professional success depends on social skills
* Research our own ideal job to see who will be our superior and our the ones in our command to see how they work together

14. Know that the culture of the work place affect performance 
* Get someone who graduated recently and make him/her told us the laboral experience
