This diary file is written by Leo Tsai E14083137 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* This course operates in a whole different mannar than moodle, so it'll take some time for me to keep up the pace.
* I really learnt a lot from the exponential growth topic last week, hoping I can develop valuable skills along the course.
* I think exponential growth in green energy will lead to a world of abundance and everlasting.
* Glasl conflict escalation model subdivides human interaction into multiple stages, which is a novel idea to me.
* Scrolling on google to find out whiat GIT is, and found out Linus stated that git can be interpreted as anything, depends on your mood.


# 2021-09-30 #

* I find this course not as stressful as other courses, and we can also cultivate invaluable skills rather than academic knowledge in the mean time.
* A good statistic can be visualiaed with vivid diagram or gragh rather than cold numb data.
* In statistics, scale is everything. Once you change the scale(axes), you can change the story.
* I learned how to cite articles correctly today, which is definitely useful in all directions.
* Newsvoice topic: How to be free from mental conditioning caused by society
* Humans copy what other people are doing and it happens through repetitive content.
* The average person thinks that they get married at a certain age.
* Take a nine-to-five job instead of starting one's own buissness.
* The subconscious manufactures your behavior through analyzing the repetitive content in your environment.
* The solution is to hack your subconcious mind and exchange bad habits with healthy one.
* It's mainly a repetition fight.
* Cited from https://newsvoice.se/2021/10/jason-christoff-mental-conditioning/