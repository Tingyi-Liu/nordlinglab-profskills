This diary is written by Chris Lee E14054201. 
# 2020-09-17 #

* Demand for whole human DNA sequencing is increasing.
* According to statistics,the world is getting better.
* The TED talk brought me a new perspective to see the world.
* A lot of problems need to be solved.
* After presentation in the second week , I'm much more confident in myself.

# 2020-09-24 #

* Arial font would be the better choice than other fonts.
* The fake news propagation will pose the threat for society .
* The picture we used should attach the url underneath it.
* Don't just believe the fakenews.

# 2020-10-01 #

* No class this week. 

# 2020-10-10 #

* The presentation of fake news should be supported by the data. .
* Using ISO standard for date.
* The video talking about how the economy works，and each transaction drive the economy .
* The high quality of the animation helps me more understanding of difference between money and credit.
* Credit  created growth with transaction ，and this self-reinforcing pattern leads to economic growth.

# 2020-10-15 #

* learned about different countries development of economy.
* It's difficult to collect data about Taiwan since Taiwan issued NTD in 1949.
* Therefore, the data is much less than other countries.
* The mortgage credit of Taiwan is increasing year by year. 
* However, it's quite hard for young people to purchase real estate property.
* Learned about the fiction stories is unique to humanbeing .
* And it's how to affect our behavior .

# 2020-10-22 #

* It's interesting to know how nervous the subject is by observing the change in heart rate.
* The fictional story of sholat maghrib is intriguing.
* Learned more about president Tsai's policy on the non-nuclear Home Project and the energy distribution of Taiwan.
* The video claimed that exercising can reduce the brain disease happened .
* Although the fact that most people already knew, it's still reminded me again of the benefit of exercise.

# 2020-10-29 #

* It's very challenging to integrate other groups' content and hypothesis this week.
* Sleep deficiency is really harmful to health and well-being.
* Learned about exercise can help brain's health and decrease the illness happened.
* The way to accompany the friends suffered from depression is just listen to what he says and not to give more suggestion.
* I think the video about depression is too tendentious.
* It's time to face how serious the depression is and figure out how to deal with it.
* The stigma of depression would make those who suffered from it unwilling to get help.

# 2020-11-05 #

* Learned about many thinking skills in this course. 
* Belonging, purpose, transcendence, and Storytelling are four pillars of a meaningful life.
* Chatting with grandma often is a very useful guide for happiness.
* Talking more about depression, how to spot, interact with, seek help for friends suffered from it.  
* I think that depression is a complex disease and cannot be cured only by medicine. 
* But it still have some way to feel better when we suffered from it .
* I still feel confused about the tricks of the video.

# 2020-11-12 #

* It's very interesting to discuss about Universal Basic Income(UBI).
* For me, the benefit of UBI is lowering my financial pressure and focusing on what I interested in.
* I think that kibbutz community(Hebrew:קִבּוּץ) in Israel is the best example for socialism So far.
* It's very challenging to figure out the monetary system on how to work but it's very useful.

# 2020-11-19 #

* Discussed the difference of the law system between U.K. and France.
* I don't figure out who makes the law in different countries until this week.
* Most students support euthanasia in this class.
* 3D printed kidneys can solve the problem with people who suffer from chronic kidney disease.

# 2020-11-26 #

* It is interesting to do cellphone addiction experiment .
* I felt incomfortable and disconnected without smartphone .
* But ,after a while , I felt more concentrated without disturbing.

# 2020-12-03 #

* The concept of the boundary is a new idea I never thought of.
* There are huge differences of news between countries.
* I think CO2 increased year by year is the serious problem we need to encounter.

# 2020-12-10 #

* Eating vegetarian 3 days per week is quite hard for a meatatarian to implemente in life.
* In order to slow down the rate of climate change,eating some vegens is a good way to reduce the carbon emission.
* I think eating vegans for the earth or for Gut health is more reasonable than for religion.
* It's a interesting to deeply discuss the act we provided to save the world.
* Watching the video about 3rd industrial revoluton to found some problems we need to solve.
* The most severe problem is economic inequality.

# 2020-12-17 Thu #

* A. Successful and productive
* B. I felt successful because I attend the most successful class  .
     I was productive because I  organize my computer and made to-do list in advance .
* C. I will have time to study tomorrow and probably will be more productive.

# 2020-12-18 Fri #

* A. Successful and unproductive
* B. I felt successful because I cooperate with other members to finish project discuss .
     I was unproductive because I do not accomplish the goal I set .
* C. I will have time to study tomorrow and probably will be more productive.

# 2020-12-19 Sat #
 
* A. Unsuccessful and unproductive
* B. I felt unsuccessful because i feel stressed  all day . I was unproductive because I do nothing all day.
* C. I will have time to study tomorrow and probably will be more productive.

# 2020-12-20 Sun #

* A. Successful and productive
* B. I felt successful because I help family clean the new house . I was productive because I accomplished an online course test .
* C. I will have time to study tomorrow and probably will be more productive.

# 2020-12-21 Mon #

* A. Successful and productive
* B. I felt successful because I catched the train in time. I was productive because I do a lot of cardio exercise.
* C. I will have time to study tomorrow and probably will be more productive.


# 2020-12-22 Tue #

* A. Unsuccessful and unproductive
* B. I felt unsuccessful because i feel stressed  all day . I was unproductive because I dom not finish the plan today.
* C. I will have time to study tomorrow and probably will be more productive.

# 2020-12-23 Wed #

* A. Successful and productive
* B. I felt successful because I do a great presentation on patent invention . 
     I was productive because I spent much time on preparing final presentation.
* C. I will have time to study tomorrow and probably will be more productive.

# 2020-12-24 Thu #

* Be confident
* Learning how to balancing life
* Do not be afraid of failure 
* Believing in my capacity to succeed
* Cooperate with others 
* There are five tips for success

* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I feel uncomfortable and dizzy all day . I was unproductive because I sleep half of the day.
* C. I will be more efficient and more productive after a long rest.

# 2020-12-25 Fri #

* A. Successful and productive.
* B. I felt successful because I ate pizza with friends to celebrate christmas. I was productive because I do one hour workout.
* C. I will probably be more productive with in good mood.

# 2020-12-26 Sat #

* A. Successful and productive
* B. I felt successful because I got high score on the midterm . I was unproductive because I went home and hanged out with friends.
* C. I will have time to study tomorrow and probably will be more productive

# 2020-12-27 Sun #

* A. Successful and unproductive
* B. I felt successful because I cooperate with others and solve the problems for fluid mechanics term project. 
     I was unproductive because I took much time on my phone and worried about what if I messed up a test.
* C. I will have time to study tomorrow and probably will be more productive.

# 2020-12-28 Mon #

* A. Successful and unproductive
* B. I felt successful because I clean my room before a midterm. At that time, I felt I was the most successful guy in the world.
     But I was unproductive because I took a lot of time to deal with chores.
* C. I will probably be more productive and more effective in an organized study space.

# 2020-12-29 Tue #

* A. Successful and productive
* B. I felt successful and energized because I woke up very early. I was productive because I do much more exercises for preparing midterm .
* C. I will probably be more productive and more effective with well sleep quality.

# 2020-12-30 Wed #

* A. Successful and unproductive
* B. I felt successful because I already passed one course. 
     I was unproductive because I underestimated the time I spend on project,causing other thing be delayed .
* C. I will probably try new time management to lighten the burden . 

# 2020-12-31 #

* Preparing the exam with other 10 people,5 questions per person.
* And we discussed the questions which had no clear answer.


# 2021-01-07 #

* The course today we have is really interesting to discuss how to find the partner .
* We came out several ways to find a proper partner noadays,including online dating , social circle expandig ,etc.
* One of the gain from this course is disscusing different topic to connect the people.
* The most thing I learned from this course is to collect statistics and to persuade audiences with data and graphs. 
