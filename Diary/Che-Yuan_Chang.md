#### Professional skills for engineering the third industrial revolution - 第三次工業革命之工程專業技能 2021

---

##### Lecturer: Professor Torbjörn Nordling. Department of Mechanical Engineering #####
Email: [professor@nordlinglab.org](mailto:professor@nordlinglab.org) Office Phone: +886 (0)6 275 7575 Ext.62164

Office: Department of Mechanical Engineering National Cheng Kung University Office 91715 on 7th floor

##### Teacher Assistant: Ric Tu #####

Email: [ric.tu@nordlinglab.org](ric.tu@nordlinglab.org)

##### Tutor: Austin Su #####

Email: [austin.su@nordlinglab.org](austin.su@nordlinglab.org) Office Phone: +886 (0)6 275 7575 Ext.62159-73

Office: Mech. Eng. building room 91A02

> Course Web 

---

 -.Nordling Lab Web page: [https://www.nordlinglab.org/profskills/#](https://www.nordlinglab.org/profskills/#) 

 -.Nordling Lab Google Drive: [NordlingLab_Course_ProfSkills](https://drive.google.com/drive/folders/1qj8ECrgVRedn-.ctFGYLgXN190btKL6pJO)

 -.Nordling Lab Google Sheets (Group list): [NordlingLab_Course_ProfSkills_ParticipantsInfo](https://docs.google.com/spreadsheets/d/1FJ47smHB1JHGm_l5yGB0FVr8EQMDhTEKzvfjo4xibE0/edit#gid=0)

##### File title: __After class diary__ 

---

Recoder: Che-Yuan Chang. Student ID: F44085305. Department of Aeronautics and Astronautics

Contact: [Chang555cheyuan@gmail.com](mailto:Chang555cheyuan@gmail.com) (__Proper Work Only Please__)

__Diary Content__

---

#### Date: 2021-09-16 Lecture.1: Introduction.(Unnecessary) ####

> Bullet Point 

  * __Knowing Basics information__ about the lectures, Professor's work experience, example research which do in the lab.
  * I __know too little__, I __should__ spend some time looking what is happen out there. 
  * If there is __question be brave to ask__ or else your are the one who will face more __obstacle__.
  * Ideas and Solution are not pop up with no reason, it comes with __observing__ problem, Surounding and life experience combine with your knowledge. 
  * Need to __pratice digest large amount of information is very short preriod of time__.
  * Need to Learn how to be more __open mind__, __creative__, learn some good __idea__ and __concept__ from __other people__.
  * Need to find a good __motivation__ and __inspiration__. 

Video watch: [HOW ABUNDANCE WILL CHANGE THE WORLD - Elon Musk 2017](https://www.youtube.com/watch?v=AgkM5g_Ob-w)

#### Date: 2021-09-23 Lecture.2 ####

 > Bullet Point 
 
  * Team work "__conflict condition__" problem solving method.  
  * Roughly know about GIThub, use Git through website for writing after class diary.
  * If there is difficulty to reach your teammate, you __Better__ to do all the jobs alone, no matter how difficult it is. 
  * __Standard of offical presentation slide oganization, copyright, reference and other precaution. __
  * Is good to __learn__ mistake now also try to __avoid__ it in the same time.
  * Try to __fully use__ your __tool to learn__ more useful information and follow up what is happen in the world.
  * Learn to collect real data for analysis the up going situation. 
  
Video watch: [Is the world getting better or worse? A look at the numbers | Steven Pinker](https://www.youtube.com/watch?v=yCm9Ng0bbEQ)

#### Date: 2021-09-30 Lecture.3 ####

##### Note #####

Statistic mean council of state, statesman or politicain, to better measure the population in the order to better serve it, so we need these goverment numbers but also havr to move beyond either blindly accepting or blindly rejecting these data.

__3 ways to spot a bad statistic:__ 

1. Can you see the uncertainty?        

2. Can I see my self in Data? (How you look the context)

3. How was data collected?  

Data is often better than you think, many people say data is bad, there is an uncertainty margin but we can see the difference are much bigger than the weakness of the data.

A big picture of statistic average is only for just reference for the big picture, if we look deeply into the detail will tell us inside of a big area have a lot of different in it, the sub area also can break down and so on, 
there is tremendous variation with in statistic data collection, which we very offten make that its equals everything.

This tell us, when we do some decision making we can not just us average to cover everything, we should look more detail find the different in each sub-area to make the correct or suitable policy within the area. 

Hans Rosling idea about the United statitic collection data ne is too idealist for present day however it may be possible in the future(now we have Google Trends),
in the present day this must be well organize, must give fair amount of payment for group of people who do these huge data collection until it can be automatically collected,
there is possible for giving partial free statistic data collection becasue these data is very valuable for soe people specially for big company for the future benefit, each data is only useful to specific person only, there is more space for discusion.

Christiane Amanpour wish the online platform can create a types of Algorithm that can filter out the fake news, it is quite difficult to filter out the fake news source, there is countless of fake news source on ther other hand is very easy to filter out
the information that goverment do what people to see, the most successful case is China and North Korea, the person who are in the power can easily control the new in the area they have control with, the fake news also happen pretty common in Taiwan, Thailand, 
Japan, korean, etc..., present day is extremely difficult to against the fake news, becasue we are not in control, some group of people make some new online platform to help people against this fake news even so it is very limited, with goverment and these people,
the best we online can do 90%, with the help of artificial automatic it maybe a very successful to control of fake news, or the opposite things will happen, then every country will be like china, we dont know, as a human we dont know what will happen who get the
power to be in control of something, just like the case of facebook which is suspect to sell privacy information to other company, to be optimistic, the futrue may be bright.

> Bullet Point 

  * If we dont have any Statistic data, how can we Observe discimination, let alone fix it, not just about discimination, is about everything.
  * One way to make numbers more accurate is to have as many people as possible be able to question them.         
  * Goverment data collection is more accurate than the private company because their __Focus__ and __Power__ are __Different__.(__Not Always__)
  * Statistic data may not be 100% accurate still it is __very close to the accuratcy__.(if it was done by the proper creator)
  * __A good statistic collection can help us see how is the prsent world are changing, letting us see the fact that the world is not like how we think it is.__
  * Is __Dangerous__ to use the average data in a big area because such a lot difference within the subarea. 
  * How accurate is the statistic is depend on how you look and use the data you have.
  * Fake News mostly likely to be __Too Dramatic, Emotional and Click-bait__, the __True sometime is Borning__.
  * __Question with Reason__ to the information you see that is not in your profession.
  * The only way to filter or keep us from fake news in present, is depend on our own __Experience, Knowledge and Logic__.
  * Is very easy to miss lead people is by giving them what they want to see, that is Human Nature.
  * Is very difficult to let people do there own research to seek the true and avoid fake new, because most of the present day people are __too Lazy to read a long information by their own__.
  * __Don't forget history is wrote by the survivor, news can be change or erase by Money and Power__. 
  * One miss leading information can cause a domino effect miss leading to the society. 
  
Video watch: [3 ways to spot a bad statistic | Mona Chalabi](https://www.youtube.com/watch?v=Zwwanld4T1w)

Video watch: [The best stats you've ever seen - Hans Rosling](https://www.youtube.com/watch?v=usdJgEwMinM&t=911s) 

Video watch: [How to seek truth in the era of fake news | Christiane Amanpour](https://www.youtube.com/watch?v=iU1bhHeCkoU&t=602s)

Video watch:[Inside the fight against Russia's fake news empire | Olga Yurkova](https://www.ted.com/talks/olga_yurkova_inside_the_fight_against_russia_s_fake_news_empire#t-90580)

Video watch:[Debunking A Century of War Lies](https://www.youtube.com/watch?v=6y0RmLLjpHw)

#### Date: 2021-10-07 Lecture.4 ####

> Bullet Point 

  * The second lecture was a little borring because I had seen Steven Pinker's TED talk.
  * Why are houses becoming more expensive when all technical products are becoming cheaper?
  * I am really inspired and puzzled by the exponential growth in data and narrow AI.
  * The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is helpful.
  * Now I understood that I should write the diary entry for all weeks in this same file.
