This diary file is written by Yong Jia Tan 陳勇嘉 E14085066 in the course Professional Skills for Engineering the Third Industrial Revolution.

# 2021-09-23 #

* The second lecture starts with an introduction on conflicts. It allows me to have a better understanding about how conflicts can be organized into different levels.
* Learned about GITHub and how it could be used as a platform to discuss and coordinate work.
* Learned about **Markdown**, which is commonly seen on Whatsapp for my case. Useful as an emphasis on certain terms.
* Acknowledged that there are a lot of objects in our surroundings that follow the rule of exponential growth leads to a world of abundance.

# 2021-09-30 #
* The third lecture placed an emphasis on statistics, which is a useful tool to allow people to know about trends and the world around them.
  - Main problem with statistics is that it could be easily manipulated by only obtaining desirable data or skewing the results of the data to suit certain contradictory conclusions.
  - Everyone should have the ability to differeciate between good data and bad data, not just believing every data that you see. Try to do your own research.
* Fake news is all over the place due to people who are lazy to fact-check the information that they received.
  - The same thing applies as statistics, be vary of any information presented and try to do your own research.